# Tes Lando

This installs a Drupal 8 instance that requires https://gitlab.com/oliverpolden/the_tes_payroll_test which is included by composer.

## Pre-requisites

 1. Install Lando: **https://docs.devwithlando.io/installation/installing.html**

## Installation

 1. Clone this repository: **git clone https://gitlab.com/oliverpolden/tes-lando.git**
 1. Go into tes-lando directory: **cd tes-lando**
 1. Run: **lando composer create-project drupal-composer/drupal-project  --stability dev --no-interaction drupal** and wait for it to finish.
 1. Run: **mv drupal/\* . && mv drupal/.\* . && rmdir drupal**
 1. Run: **composer install** and wait for it to finish.
 1. Run: **lando start** and wait for it to finish.
 1. Run: **lando info**
    - Visit one of the "urls" specified by **lando info** to install Drupal in your browser
    - Use information under database *hostnames* and *creds* specified by **lando info** for the database connection
 1. At configure site screen you can use any details you wish
 1. Visit: **Extend**
 1. Install the **The Tes Payroll Test** module
 
## Viewing the Payroll page

 1. In the **Tools** menu there is a **Payroll Dates** link or visit **/payroll/dates**
 
## Running Unit tests

 1. Visit: **Extend**
 1. Install the *Testing* module
 1. Visit: Configuration > Development > Testing
 1. Search for **Payroll**
 1. Check the **Class PayrollDatesTest** test
 1. Run tests